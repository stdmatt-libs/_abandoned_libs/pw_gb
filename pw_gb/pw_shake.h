#ifndef  __PW_SHAKE_H__
#define  __PW_SHAKE_H__

// pw
#include "pw_defs.h"


//----------------------------------------------------------------------------//
// Vars                                                                       //
//----------------------------------------------------------------------------//
extern I8   Shake_x;
extern I8   Shake_y;
extern BOOL Shake_IsShaking;


//----------------------------------------------------------------------------//
// Public Functions                                                           //
//----------------------------------------------------------------------------//
void Shake_Reset (U8 frames_to_shake, U8 loop_count, U8 max_x, U8 max_y);
void Shake_Update();
void Shake_Stop  ();

#endif // __PW_SHAKE_H__
