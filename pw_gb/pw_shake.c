// Header
#include "pw_shake.h"


//----------------------------------------------------------------------------//
// Constants                                                                  //
//----------------------------------------------------------------------------//
#define SHAKE_ANIMATION_LOOP_MAX 8
#define SHAKE_ANIMATION_FRAMES   2


//----------------------------------------------------------------------------//
// Vars                                                                       //
//----------------------------------------------------------------------------//
// Public
I8   Shake_x;
I8   Shake_y;
BOOL Shake_IsShaking;

// Private;
static U8 _Shake_curr_frame;
static U8 _Shake_total_frames;
static U8 _Shake_loop_frames;
static U8 _Shake_max_x;
static U8 _Shake_max_y;


//----------------------------------------------------------------------------//
// Functions                                                                  //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
Shake_Reset(U8 frames_to_shake, U8 loop_count, U8 max_x, U8 max_y)
{
    // Public Stuff
    Shake_x         = 0;
    Shake_y         = 0;
    Shake_IsShaking = TRUE;

    // Private Stuff.
    _Shake_curr_frame   = 0;
    _Shake_total_frames = frames_to_shake * loop_count;
    _Shake_loop_frames  = frames_to_shake;
    _Shake_max_x        = max_x;
    _Shake_max_y        = max_y;

}

//------------------------------------------------------------------------------
void
Shake_Update()
{
    if(!Shake_IsShaking) {
        return;
    }

    ++_Shake_curr_frame;
    if(_Shake_curr_frame >= _Shake_total_frames) {
        Shake_IsShaking = FALSE;
        Shake_x         = 0;
        Shake_y         = 0;
    } else if(_Shake_curr_frame % _Shake_loop_frames == 0) {
        Shake_x = Random() % _Shake_max_x;
        Shake_y = Random() % _Shake_max_y;
    }
}

//------------------------------------------------------------------------------
void
Shake_Stop()
{
    Shake_IsShaking = FALSE;
}
